package com.example.rental.service.city;

import com.example.rental.entites.City;

import java.util.List;

public interface CityService {
    List<City> getAll();
}
