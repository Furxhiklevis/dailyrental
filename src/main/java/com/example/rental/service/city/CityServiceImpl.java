package com.example.rental.service.city;

import com.example.rental.entites.City;
import com.example.rental.repository.CityRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService{
    protected final CityRepository repository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.repository = cityRepository;
    }

    @Override
    public List<City> getAll() {
        return repository.findAll(Sort.sort(City.class).ascending());
    }
}
