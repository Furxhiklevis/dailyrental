package com.example.rental.service.car;

import com.example.rental.entites.Car;

import java.util.List;
import java.util.UUID;

public interface CarService {
    List<Car>  getFeaturedCars();
    List<Car>  getRecentCars();
    void createDummyData();

    Car getProductById(UUID id);
}

