package com.example.rental.service.car;

import com.example.rental.entites.Car;
import com.example.rental.entites.City;
import com.example.rental.repository.CarRepository;
import com.example.rental.repository.CityRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final CityRepository cityRepository;

    public CarServiceImpl(CarRepository carRepository, CityRepository cityRepository) {
        this.carRepository = carRepository;
        this.cityRepository = cityRepository;
    }

    @Override
    public List<Car> getFeaturedCars() {
        return carRepository.getFeaturedCars();
    }

    @Override
    public List<Car> getRecentCars() {
        return carRepository.getRecentCars();
    }

    @Override
    public Car getProductById(UUID id) {
        return carRepository.findById(id).orElse(null);
    }

    @Override
    public void createDummyData() {
        Optional<City> tirana = cityRepository.findByNameEquals("Tirana");
        if (tirana.isEmpty()) {
            City tr = City.builder().name("Tirana").build();
            cityRepository.save(tr);
            tirana  = cityRepository.findByNameEquals("Tirana");
        }

        Optional<City> korca = cityRepository.findByNameEquals("Korca");
        if (korca.isEmpty()) {
            City kc = City.builder().name("Korca").build();
            cityRepository.save(kc);
            korca = cityRepository.findByNameEquals("Korca");
        }

        Optional<City> durres = cityRepository.findByNameEquals("Durres");
        if (durres.isEmpty()) {
            City dr = City.builder().name("Durres").build();
            cityRepository.save(dr);
        }

        Optional<Car> firstCar = carRepository.getByProducerAndModelAndYear("Benz Mercedes", "C-Klass", 2010);
        if (firstCar.isEmpty()) {
            Car c = Car.builder()
                    .cityId(tirana.get().getId())
                    .description("Qui et explicabo voluptatem et ab qui vero et voluptas. Sint voluptates temporibus quam autem. Atque nostrum voluptatum laudantium a doloremque enim et ut dicta. Nostrum ducimus est iure minima totam doloribus nisi ullam deserunt. Corporis aut officiis sit nihil est. Labore aut sapiente aperiam.\n" +
                            "                            Qui voluptas qui vero ipsum ea voluptatem. Omnis et est. Voluptatem officia voluptatem adipisci et iusto provident doloremque consequatur. Quia et porro est. Et qui corrupti laudantium ipsa.\n" +
                            "                            Eum quasi saepe aperiam qui delectus quaerat in. Vitae mollitia ipsa quam. Ipsa aut qui numquam eum iste est dolorum. Rem voluptas ut sit ut.")
                    .featured(true)
                    .producer("Benz Mercedes")
                    .model("C-Klass")
                    .year(2010)
                    .pricePerDay(60d)
                    .image("mercedes-ml.jpg")
                    .build();
            carRepository.save(c);
        }

        Optional<Car> secondCar = carRepository.getByProducerAndModelAndYear("Benz Mercedes", "E-Klass", 2015);
        if (secondCar.isEmpty()) {
            Car c = Car.builder()
                    .cityId(tirana.get().getId())
                    .description("Qui et explicabo voluptatem et ab qui vero et voluptas. Sint voluptates temporibus quam autem. Atque nostrum voluptatum laudantium a doloremque enim et ut dicta. Nostrum ducimus est iure minima totam doloribus nisi ullam deserunt. Corporis aut officiis sit nihil est. Labore aut sapiente aperiam.\n" +
                            "                            Qui voluptas qui vero ipsum ea voluptatem. Omnis et est. Voluptatem officia voluptatem adipisci et iusto provident doloremque consequatur. Quia et porro est. Et qui corrupti laudantium ipsa.\n" +
                            "                            Eum quasi saepe aperiam qui delectus quaerat in. Vitae mollitia ipsa quam. Ipsa aut qui numquam eum iste est dolorum. Rem voluptas ut sit ut.")
                    .featured(true)
                    .producer("Benz Mercedes")
                    .model("E-Klass")
                    .year(2015)
                    .pricePerDay(70d)
                    .image("mercedes-ml.jpg")
                    .build();
            carRepository.save(c);
        }

        Optional<Car> fifth = carRepository.getByProducerAndModelAndYear("BMW", "Seria 7", 2021);
        if (fifth.isEmpty()) {
            Car c = Car.builder()
                    .cityId(tirana.get().getId())
                    .description("Qui et explicabo voluptatem et ab qui vero et voluptas. Sint voluptates temporibus quam autem. Atque nostrum voluptatum laudantium a doloremque enim et ut dicta. Nostrum ducimus est iure minima totam doloribus nisi ullam deserunt. Corporis aut officiis sit nihil est. Labore aut sapiente aperiam.\n" +
                            "                            Qui voluptas qui vero ipsum ea voluptatem. Omnis et est. Voluptatem officia voluptatem adipisci et iusto provident doloremque consequatur. Quia et porro est. Et qui corrupti laudantium ipsa.\n" +
                            "                            Eum quasi saepe aperiam qui delectus quaerat in. Vitae mollitia ipsa quam. Ipsa aut qui numquam eum iste est dolorum. Rem voluptas ut sit ut.")
                    .featured(true)
                    .producer("BMW")
                    .model("Seria 7")
                    .year(2021)
                    .pricePerDay(140d)
                    .image("bmw.jpg")
                    .build();
            carRepository.save(c);
        }

        Optional<Car> thirdCar = carRepository.getByProducerAndModelAndYear("Benz Mercedes", "C-Klass", 2019);
        if (thirdCar.isEmpty()) {
            Car c = Car.builder()
                    .cityId(korca.get().getId())
                    .description("Qui et explicabo voluptatem et ab qui vero et voluptas. Sint voluptates temporibus quam autem. Atque nostrum voluptatum laudantium a doloremque enim et ut dicta. Nostrum ducimus est iure minima totam doloribus nisi ullam deserunt. Corporis aut officiis sit nihil est. Labore aut sapiente aperiam.\n" +
                            "                            Qui voluptas qui vero ipsum ea voluptatem. Omnis et est. Voluptatem officia voluptatem adipisci et iusto provident doloremque consequatur. Quia et porro est. Et qui corrupti laudantium ipsa.\n" +
                            "                            Eum quasi saepe aperiam qui delectus quaerat in. Vitae mollitia ipsa quam. Ipsa aut qui numquam eum iste est dolorum. Rem voluptas ut sit ut.")
                    .featured(false)
                    .producer("Benz Mercedes")
                    .model("C-Klass")
                    .year(2019)
                    .pricePerDay(100d)
                    .image("mercedes-ml.jpg")
                    .build();
            carRepository.save(c);
        }

        Optional<Car> fourthCar = carRepository.getByProducerAndModelAndYear("BMW", "Seria 6", 2019);
        if (fourthCar.isEmpty()) {
            Car c = Car.builder()
                    .cityId(korca.get().getId())
                    .description("Qui et explicabo voluptatem et ab qui vero et voluptas. Sint voluptates temporibus quam autem. Atque nostrum voluptatum laudantium a doloremque enim et ut dicta. Nostrum ducimus est iure minima totam doloribus nisi ullam deserunt. Corporis aut officiis sit nihil est. Labore aut sapiente aperiam.\n" +
                            "                            Qui voluptas qui vero ipsum ea voluptatem. Omnis et est. Voluptatem officia voluptatem adipisci et iusto provident doloremque consequatur. Quia et porro est. Et qui corrupti laudantium ipsa.\n" +
                            "                            Eum quasi saepe aperiam qui delectus quaerat in. Vitae mollitia ipsa quam. Ipsa aut qui numquam eum iste est dolorum. Rem voluptas ut sit ut.")
                    .featured(false)
                    .producer("BMW")
                    .model("Seria 6")
                    .year(2019)
                    .pricePerDay(100d)
                    .image("bmw.jpg")
                    .build();
            carRepository.save(c);
        }
    }


}
