package com.example.rental.service.booking;

import com.example.rental.dto.BookingWrite;
import com.example.rental.dto.CarFilter;
import com.example.rental.dto.ServiceResponse;
import com.example.rental.entites.Booking;
import com.example.rental.entites.Car;
import com.example.rental.entites.City;
import com.example.rental.repository.BookingRepository;
import com.example.rental.repository.CarRepository;
import com.example.rental.repository.CityRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.print.ServiceUI;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BookingServiceImpl implements BookingService {

    protected final CityRepository cityRepository;
    protected final CarRepository carRepository;
    protected final BookingRepository bookingRepository;

    @Override
    public ServiceResponse search(CarFilter filter) {
        if (filter == null) {
            return ServiceResponse.error("Filter not valid!");
        }

        if (filter.getCityId() == null)
            return ServiceResponse.error("Please specify the city!");

        if (filter.getCheckin() == null)
            return ServiceResponse.error("Please specify checkin date!");

        if (filter.getCheckout() == null)
            return ServiceResponse.error("Please specify checkout date!");


        if (filter.getCheckin().isAfter(filter.getCheckout()))
            return ServiceResponse.error("Checkin date should be a date before checkout!");

        // Logic  to find the valid vehicles
        Optional<City> city = cityRepository.findById(filter.getCityId());

        if(city.isEmpty())
            return ServiceResponse.error("City not found in database!");

        List<Car> items = carRepository.searchBy(filter.getCityId(), filter.getCheckin(), filter.getCheckout());
        return ServiceResponse.success("The filtered infos data", items);
    }

    @Override
    public ServiceResponse doBooking(BookingWrite dto) {
        if (dto == null) {
            return ServiceResponse.error("Booking data should not be empty!");
        }

        if (dto.getId() == null)
            return ServiceResponse.error("Please specify the vehicle id!");

        if (StringUtils.isBlank(dto.getFirstName()))
            return ServiceResponse.error("First name is required!");

        if (StringUtils.isBlank(dto.getLastName()))
            return ServiceResponse.error("Last name is required!");

        if (dto.getCheckin() == null)
            return ServiceResponse.error("Please specify checkin date!");

        if (dto.getCheckout() == null)
            return ServiceResponse.error("Please specify checkout date!");

        if (dto.getCheckin().isAfter(dto.getCheckout()))
            return ServiceResponse.error("Checkin date should be a date before checkout!");

        // Logic  to find the valid vehicles
        Optional<Car> car = carRepository.findById(dto.getId());

        if(car.isEmpty())
            return ServiceResponse.error("Vehicle not found in database!");

        List<Booking> actualBookings = bookingRepository.searchBy(car.get().getId(), dto.getCheckin(), dto.getCheckout());

        if (actualBookings != null && actualBookings.size() > 0) {
            return ServiceResponse.error("The vehicle is booked in the period you choosed!");
        }


        Booking booking = Booking.builder()
                .carId(car.get().getId())
                .bookedFrom(dto.getCheckin())
                .bookedTo(dto.getCheckout())
                .cityId(car.get().getCityId())
                .cityName(car.get().getCity().getName())
                .carProducer(car.get().getProducer())
                .carModel(car.get().getModel())
                .pricePerDay(car.get().getPricePerDay())
                .createdAt(LocalDateTime.now())
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .phone(dto.getPhone())
                .carImage(car.get().getImage())
                .build();

        bookingRepository.save(booking);
        return ServiceResponse.success("Booking process finished successfully!");
    }
}
