package com.example.rental.service.booking;

import com.example.rental.dto.BookingWrite;
import com.example.rental.dto.CarFilter;
import com.example.rental.dto.ServiceResponse;
import com.example.rental.entites.Booking;

public interface BookingService {
    ServiceResponse search(CarFilter filter);
    ServiceResponse doBooking(BookingWrite dto);
}
