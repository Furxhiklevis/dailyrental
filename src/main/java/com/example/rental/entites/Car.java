package com.example.rental.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "dr_car")
public class Car {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name="uuid2", strategy = "uuid2")
    @Column(name = "id", updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    protected UUID id;

    @Column(name = "description", length = 2000)
    public String description;

    @Column(name = "producer")
    public String producer;

    @Column(name = "model")
    public String model;

    @Column(name = "image")
    public String image;

    @Column(name = "year")
    protected Integer year;

    @Column(name = "price_per_day")
    private Double pricePerDay;

    @Column(name = "featured")
    private Boolean featured = false;

    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(name = "city_id")
    protected UUID cityId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(insertable = false, updatable = false, name = "city_id")
    protected City city;
}
