package com.example.rental.entites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="dr_booking")
public class Booking {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name="uuid2", strategy = "uuid2")
    @Column(name = "id", updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    protected UUID id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "booked_from")
    private LocalDateTime bookedFrom;

    @Column(name = "booked_to")
    private LocalDateTime bookedTo;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Type(type = "org.hibernate.type.UUIDCharType")
    protected UUID carId;

    @Column(name = "car_producer")
    private String carProducer;

    @Column(name = "car_model")
    private String carModel;

    @Column(name = "car_image")
    private String carImage;

    @Column(name = "city_id")
    private UUID cityId;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "price_per_day")
    private Double pricePerDay;
}
