package com.example.rental.controller;

import com.example.rental.dto.CarFilter;
import com.example.rental.service.car.CarService;
import com.example.rental.service.city.CityService;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

@Component
public class BaseController{
    @ModelAttribute("filter")
    public CarFilter getFilter() {
        return new CarFilter();
    }


    final CarService carService;
    final CityService cityService;

    public BaseController(CarService carService, CityService cityService) {
        this.carService = carService;
        this.cityService = cityService;
    }

    public void initModelMap(ModelMap map) {
        map.addAttribute("cities", cityService.getAll());
        map.addAttribute("filter", new CarFilter());
    }
}
