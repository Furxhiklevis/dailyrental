//package com.example.rental.controller;
//
//import com.example.rental.dto.CarFilter;
//import com.example.rental.dto.ServiceResponse;
//import com.example.rental.entites.Booking;
//import com.example.rental.entites.Car;
//import com.example.rental.entites.City;
//import com.example.rental.service.booking.BookingService;
//import com.example.rental.service.city.CityService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//@Controller
//public class BookingController {
//
//    @Autowired
//    BookingService bookingService;
//    CityService cityService;
//
//
//    @PostMapping("/booking/save")
//    public String detail(ModelMap modelMap, @ModelAttribute("filter") CarFilter filter,Booking booking) {
//        ServiceResponse searchResult = bookingService.search(filter);
//        modelMap.addAttribute("cities", cityService.getAll());
//        modelMap.addAttribute("result", searchResult);
//
//        modelMap.addAttribute("cars", searchResult.getResult());
//        bookingService.save(booking);
//        return "redirect:/details";
//    }
//
//}