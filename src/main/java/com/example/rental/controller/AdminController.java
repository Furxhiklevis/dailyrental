package com.example.rental.controller;

import com.example.rental.dto.ServiceResponse;
import com.example.rental.entites.Car;
import com.example.rental.entites.City;
import com.example.rental.repository.CarRepository;
import com.example.rental.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

@Controller
public class AdminController {

    @Autowired
    CarRepository carRepo;
    @Autowired
    CityRepository cityRepository;

    @RequestMapping("/admin")
    public String home(Model model) {
        model.addAttribute("home");
        return "admin/index";
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("car", new Car());
        modelAndView.setViewName("admin/register"); // resources/templates/register.html
        return modelAndView;
    }

    @PostMapping("/register/save")
    public String save(@RequestParam("file") MultipartFile file,
                       Car car, City city,
                       Model model,
                       RedirectAttributes redirectAttributes,
                       @ModelAttribute ServiceResponse serviceResponse) {
        model.addAttribute("car", car);
        model.addAttribute("city", city);

        ServiceResponse response = ServiceResponse.builder().build();
        if (file.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("Please provide an image!");
            redirectAttributes.addFlashAttribute("car", car);
            redirectAttributes.addFlashAttribute("result", response);
            return "redirect:/register";
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        // save the file on the local file system
        try {
            String filePath = new File(".").getCanonicalPath() + "/src/main/webapp/WEB-INF/static/vehicles/" + fileName;
            Path path = Path.of(filePath);
            File finalFile = path.toFile();
            if (!finalFile.exists()) {
                finalFile.mkdirs();
                finalFile.createNewFile();
            }

            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();

            response.setSuccess(false);
            response.setMessage("Server problem when saving file");
            redirectAttributes.addFlashAttribute("car", car);
            redirectAttributes.addFlashAttribute("result", response);

            return "redirect:/register";
        }

        response.setSuccess(true);
        response.setMessage("Vehicle saved successfully");
        redirectAttributes.addFlashAttribute("result", response);
        carRepo.save(car);

        return "redirect:/register";
    }

    @RequestMapping("/list")
    public String countsList(Model model) {
        model.addAttribute("car", carRepo.findAll());
        return "admin/listuser";
    }

    @GetMapping("/update")
    public String displayEmployeeUpdateForm(@RequestParam("id") UUID id, Model model) {
      Optional<Car> car= carRepo.findById(id);
        model.addAttribute("car", car);
        return "/admin";
    }

    @GetMapping("list/delete")
    public String deleteEmployee(@RequestParam("id") UUID id, Model model,Car car) {
         carRepo.findById(id);
         carRepo.delete(car);
        return "redirect:/admin";
    }


}