package com.example.rental.controller;

import com.example.rental.service.car.CarService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/utils")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UtilsController {
    protected @NonNull CarService carService;

    @ResponseBody
    @RequestMapping(value = "/create-initial-data")
    public ResponseEntity<String> createInitialData() {
        carService.createDummyData();
        return ResponseEntity.ok("Data created successfully!");
    }
}
