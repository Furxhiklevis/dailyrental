package com.example.rental.controller;

import com.example.rental.dto.BookingWrite;
import com.example.rental.dto.CarFilter;
import com.example.rental.dto.ServiceResponse;
import com.example.rental.entites.Booking;
import com.example.rental.entites.Car;
import com.example.rental.service.booking.BookingService;
import com.example.rental.service.car.CarService;
import com.example.rental.service.city.CityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.UUID;

@Controller
@SessionAttributes({"filter"})
public class WebsiteController extends BaseController {
    private final BookingService bookingService;

    public WebsiteController(CarService carService, CityService cityService, BookingService bookingService) {
        super(carService, cityService);
        this.bookingService = bookingService;
    }

    @RequestMapping("/")
    public String home(ModelMap model, @ModelAttribute("filter") CarFilter filter) {
        initModelMap(model);
        model.addAttribute("featuredCars", carService.getFeaturedCars());
        model.addAttribute("site");
        model.addAttribute("cities", cityService.getAll());
        return "website/index";
    }

    @GetMapping("/car-detail/{id}")
    public String productDetail(@PathVariable UUID id, ModelMap modelMap) {
        Car car = carService.getProductById(id);
        modelMap.addAttribute("car", car);
        return "website/details";
    }

    @RequestMapping("/about")
    public String about(Model model) {
        model.addAttribute("site");
        return "website/about";
    }

    @RequestMapping("/contact")
    public String contact(Model model) {
        model.addAttribute("site");
        return "website/contact";
    }

    @RequestMapping("/rentals")
    public String rental(Model model, ModelMap modelMap) {
        List<Car> cars =  carService.getRecentCars();
        modelMap.addAttribute("cars", cars);
        modelMap.addAttribute("cities", cityService.getAll());
        return "website/rentals";
    }

    @PostMapping("/search")
    public String detail(ModelMap modelMap, @ModelAttribute("filter") CarFilter filter) {

        ServiceResponse searchResult = bookingService.search(filter);
        modelMap.addAttribute("cities", cityService.getAll());
        modelMap.addAttribute("result", searchResult);

        modelMap.addAttribute("cars", searchResult.getResult());
        return "website/rentals";
    }

    @GetMapping("/search/clear")
    public String clearFilter(SessionStatus status) {
        status.setComplete();
        return "redirect:/rentals";
    }

    @PostMapping("/booking/save")
    public String detail(BookingWrite dto, ModelMap modelMap, SessionStatus status, RedirectAttributes redirectAttrs) {

        ServiceResponse result = bookingService.doBooking(dto);

        if (!result.isSuccess()) {
            modelMap.addAttribute("dto", dto);
            modelMap.addAttribute("result", result);
            return productDetail(dto.getId(), modelMap);
        }

        status.setComplete();
        redirectAttrs.addFlashAttribute("result", result);

        return "redirect:/car-detail/" + dto.getId();
    }
}
