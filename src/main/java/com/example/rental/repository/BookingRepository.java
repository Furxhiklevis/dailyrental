package com.example.rental.repository;

import com.example.rental.entites.Booking;
import com.example.rental.entites.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface BookingRepository extends JpaRepository<Booking, UUID> {

    @Query(value = " select b from Booking b " +
            " where b.carId = :carId and ((:checkin > b.bookedFrom and :checkin < b.bookedTo) or (:checkout > b.bookedFrom and :checkout < b.bookedTo)) "
    )
    List<Booking> searchBy(@Param("carId") UUID carId, @Param("checkin") LocalDateTime checkin, @Param("checkout") LocalDateTime checkout);
}
