package com.example.rental.repository;

import com.example.rental.entites.Car;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarRepository extends PagingAndSortingRepository<Car, UUID> {
    @Query(nativeQuery = true, value = "SELECT * FROM dr_car p")
    List<Car> getRecentCars();

    @Query(value = "SELECT c FROM Car c where c.featured = 1")
    List<Car> getFeaturedCars();

    Optional<Car> getByProducerAndModelAndYear(String producer, String model, Integer year);

    @Query(value = "select c from Car c where c.cityId = :cityId and c.id not in (" +
            " select b.carId from Booking b " +
            " where b.carId = c.id and ((:checkin > b.bookedFrom and :checkin < b.bookedTo) or (:checkout > b.bookedFrom and :checkout < b.bookedTo)) " +
            ")")
    List<Car> searchBy(@Param("cityId") UUID cityId, @Param("checkin") LocalDateTime checkin, @Param("checkout") LocalDateTime checkout);
}
