package com.example.rental.dto;

import lombok.*;

import javax.persistence.Lob;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceResponse<T> {
    protected boolean success;
    protected String message;
    protected T result;

    public static ServiceResponse error(String message) {
        return ServiceResponse.builder().success(false).message(message).build();
    }

    public static ServiceResponse success(String message) {
        return ServiceResponse.builder().success(true).message(message).build();
    }

    public static <T> ServiceResponse success(String message, T result) {
        return ServiceResponse.builder()
                .success(true)
                .result(result)
                .message(message).build();
    }

    public static <T> ServiceResponse success(T result) {
        return ServiceResponse.builder()
                .success(true)
                .result(result)
                .build();
    }
}
